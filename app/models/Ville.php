<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Ville extends Eloquent {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ville';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/*
	* Fonction qui récupère les quartiers de la ville
	*/
	public function quartiers(){
		return $this->hasMany('Quartier','ville_id');
	}

}
?>