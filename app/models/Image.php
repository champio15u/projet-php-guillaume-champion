<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Image extends Eloquent {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'image';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/*
	* Fonction qui récupère l'annonce de l'image
	*/
	public function annonce(){
		return $this->belongsTo('Annonce','annonce_id');
	}

}
?>