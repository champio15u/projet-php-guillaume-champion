<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class TypeAnnonce extends Eloquent {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'typeannonce';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/*
	* Fonction qui récupère les annnonces en fonction du type d'annonce
	*/
	public function annonces(){
		return $this->hasMany('Annonce','typeAnn_id');
	}

}
?>