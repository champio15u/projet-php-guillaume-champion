<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Internaute extends Eloquent {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'internaute';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/*
	* Fonction qui récupère l'annonce de l'image
	*/
	public function annonces(){
		return $this->hasMany('Annonce','inter_id');
	}

}
?>