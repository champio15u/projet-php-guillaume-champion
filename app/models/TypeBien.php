<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class TypeBien extends Eloquent {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'typebien';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/*
	* Fonction qui récupère les annonces en fonction du type de bien
	*/
	public function annonces(){
		return $this->hasMany('Annonce','typeBien_id');
	}

}
?>