<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class Quartier extends Eloquent {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'quartier';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/*
	* Fonction qui récupère les annonces en fonction d'un quartier
	*/
	public function annonces(){
		return $this->hasMany('Annonce','quartier_id');
	}

	/*
	* Fonction qui récupère la ville d'un quartier
	*/
	public function ville(){
		return $this->belongsTo('Ville','ville_id');
	}

}
?>