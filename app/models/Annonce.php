<?php

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\ModelNotFoundException as Erreur;

class Annonce extends Eloquent {
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'annonce';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/* 
	* Fonction qui récupère le type de l'annnonce
	*/
	public function typeAnnonce(){
		return $this->belongsTo('TypeAnnonce','typeAnn_id');
	}

	/* 
	* Fonction qui récupère le type de bien de l'annonce
	*/
	public function typeBien(){
		return $this->belongsTo('TypeBien','typeBien_id');
	}

	/* 
	* Fonction qui récupère l'internaute qui a déposé l'annonce
	*/
	public function internaute(){
		return $this->belongsTo('Internaute','inter_id');
	}

	/* 
	* Fonction qui récupère le quartier de l'annonce
	*/
	public function quartier(){
		return $this->belongsTo('Quartier','quartier_id');
	}

	/* 
	* Fonction qui récupère les images de l'annonce 
	*/
	public function images(){
		return $this->hasMany('Image','annonce_id');
	}



}

?>
