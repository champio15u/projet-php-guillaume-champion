﻿<?php 

//redirection vers page d'accueil
$app->get('/', function () use ($app) {
	 $app->redirect($app->urlFor('home'));
});

// Page d'accueil
$app->get('/home', function () use ($app) {
	$annonces = Annonce::with("images")->with("quartier")->get();
	
	$villes = Ville::all();

 	if(count($annonces) ===  0){
 		$app->redirect($app->urlFor('notifi', array(
 			'msg' => 'Pas d\'annonce disponible'
 		)));
 	}
    $app->render('accueil.html', array(
    	'annonces' => $annonces,
    	'villes' => $villes
    ));
})->name("home");


// Page de depot d'annonce
$app->get('/annonce', function () use ($app) {
	$typeAnn = TypeAnnonce::all();
	$typeBien = TypeBien::all();
	$quartier = Quartier::all();

    $app->render('ajoutAnnonce.html',array(
    	'typeAnn' => $typeAnn,
    	'typeBien' => $typeBien,
    	'quartiers' => $quartier
    ));
});



$app->get('/annonce:id', function ($id) use ($app){
	// Récupérer l'annonce grâce à l'annonce
	if(filter_var($id, FILTER_VALIDATE_INT)){
		$annonce = Annonce::find($id);
		$quartiers = Quartier::with("ville")->get();
		
	} else {
		 $app->redirect($app->urlFor('notifd', array(
 		  	'msg' => 'Page invalide'
 		  )));
	} 
	$annonce->load("images","typeBien","typeAnnonce","internaute","quartier");
	

	$app->render('displayAnnonce.html', array(
		'annonce' => $annonce,
		'quartiers' => $quartiers
	));

});


// Page de recherche 

$app->get('/recherche', function () use ($app){

	$typeB=  TypeBien::all();
	$typeAnn = TypeAnnonce::all();
	$ville = Ville::all();
	$quartier = Quartier::all();

	$app->render('rechercheForm.html',compact('typeB','typeAnn','ville','quartier'));

})->name('recherche');


// Resultat recherche

$app->post('/resultRecher', function () use ($app){

	
	// recuperation et filtrage des données

	$motcle = filter_var($_POST["motCle"], FILTER_SANITIZE_STRING);

	$reqSQL = "(titre LIKE ? OR contenu like ?)";
	$tab = array("%$motcle%","%$motcle%");

	$typeAnn = filter_var($_POST["typeAnn"], FILTER_SANITIZE_NUMBER_INT);
	if($typeAnn > 0 ){
		$reqSQL.=" and typeAnn_id = ?";
		array_push($tab,$typeAnn);
	}

	$typeBien = filter_var($_POST["typeBien"],FILTER_SANITIZE_NUMBER_INT);
	if($typeBien > 0 ){
		$reqSQL.=" and typeBien_id = ?";
		array_push($tab,$typeBien);
	}

	$quartier = filter_var($_POST["quartier"],FILTER_SANITIZE_NUMBER_INT);
	 if($quartier > 0){
		$reqSQL.=" and quartier_id = ?";
		array_push($tab,$quartier);
	}

	$prixMin =filter_var( $_POST["prix_min"],FILTER_SANITIZE_NUMBER_INT);
	$prixMax = filter_var($_POST["prix_max"],FILTER_SANITIZE_NUMBER_INT);


	if($prixMin > -1 && $prixMax > -1){
		if($prixMin < $prixMax){
			$reqSQL.=" and prix between ? and ?";
			array_push($tab,$prixMin,$prixMax);
		}
	} else {
		if($prixMin > -1){
			$reqSQL.=" and prix >= ?";
			array_push($tab,$prixMin);
			
		} elseif($prixMax > -1){
			$reqSQL.=" and prix <= ?";
			array_push($tab,$prixMax);
		}
	}


	$loyerMin = filter_var($_POST["loyer_min"],FILTER_SANITIZE_NUMBER_INT);
	$loyerMax = filter_var($_POST["loyer_max"],FILTER_SANITIZE_NUMBER_INT);

	if($loyerMin > -1 && $loyerMax > -1){
		if($loyerMin < $loyerMax){
			$reqSQL.=" and loyer between ? and ?";
			array_push($tab,$loyerMin,$loyerMax);
		} 
	} else {
		if($loyerMin > -1){
			$reqSQL.=" and loyer >= ?";
			array_push($tab,$loyerMin);
		} elseif($loyerMax > -1){
			$reqSQL.=" and loyer <= ?";
			array_push($tab,$loyerMax);
		} 
	}

	$supMin = filter_var($_POST["superf_min"],FILTER_SANITIZE_NUMBER_INT);
	$supMax = filter_var($_POST["superf_max"],FILTER_SANITIZE_NUMBER_INT);

	
	if($supMin > -1 && $supMax > -1){
		if($supMin < $supMax){
			$reqSQL.=" and superficie between ? and ?";
			array_push($tab,$supMin,$supMax);
		} 
	} else {
		if($supMin > -1){
			$reqSQL.=" and superficie >= ?";
			array_push($tab,$supMin);
		} elseif($supMax > -1){
			$reqSQL.=" and superficie <= ?";
			array_push($tab,$supMax);
		} 
	}

	$nbPieces = filter_var($_POST["nbPiece"],FILTER_SANITIZE_NUMBER_INT);
	if($nbPieces > 0 ){
		if($nbPieces == 8 ) {
			$reqSQL.= " and nbPiece >= ?";
			array_push($tab,$nbPieces);
		} else {
			$reqSQL.=" and nbPiece = ?";
			array_push($tab,$nbPieces);
		}
	} 
	
	// Recherche dans la base 
	$annonces = Annonce::with("quartier")->with("images")->whereRaw($reqSQL, $tab)->get();


	// ré-affichage de la page de recherche
	$typeB=  TypeBien::all();
	$typeAnn = TypeAnnonce::all();
	$ville = Ville::all();
	$quartier = Quartier::all();

	$app->render('resultRecher.html',compact('typeB','typeAnn','ville','quartier','annonces'));


})->name('resultRecher');



 // Envoie du depot de l'annonce
$app->post('/notifDepot', function () use ($app) {
	
	// Verification des données du formulaire
	// ----------------------------------------
	if( !empty($_POST) ){

		// -------------- Titre ----------------
		if(!empty($_POST['titre'])){
			$titre = filter_var($_POST['titre'], FILTER_SANITIZE_STRING);
		} else {
		    $app->redirect($app->urlFor('notife', array(
	 		  	'msg' => 'Titre non spécifié'
 		  	)));
		}

		// -------------- Contenu -------------
		if(!empty($_POST['contenu'])){
			$contenu = filter_var($_POST['contenu'], FILTER_SANITIZE_STRING); 
		} else {
		     $app->redirect($app->urlFor('notife', array(
	 		  	'msg' => 'Contenu non spécifié'
 		  	)));
		}
		// ------------- Prix -----------------
 		if(isset($_POST['prix']) && filter_var($_POST['prix'], FILTER_VALIDATE_INT)){
			$prix = filter_var($_POST["prix"],FILTER_SANITIZE_NUMBER_INT);
		} else {
		     $app->redirect($app->urlFor('notife', array(
	 		  	'msg' =>  'Prix non spécifié'
 		  	)));
		}

		// ------------- Loyer ----------------
		if(isset($_POST['loyer']) && filter_var($_POST['loyer'], FILTER_VALIDATE_INT)){
			$loyer = filter_var($_POST["loyer"],FILTER_SANITIZE_NUMBER_INT);
		} else {
			  $app->redirect($app->urlFor('notife', array(
	 		  	'msg' => 'Loyer non spécifié'
 		  	)));

		}
		
		//---------- Nombre de pieces -----------
		if(isset($_POST['nbPieces']) && filter_var($_POST['nbPieces'], FILTER_VALIDATE_INT)){
			$nbPieces = filter_var($_POST["nbPieces"],FILTER_SANITIZE_NUMBER_INT);
		} else {
		     $app->redirect($app->urlFor('notife', array(
	 		  	'msg' => 'Nombre de piece non spécifié'
 		  	)));
		} 

		// --------- Superficie -------------
		if(isset($_POST['superficie']) && filter_var($_POST['superficie'], FILTER_VALIDATE_FLOAT)){
			$superficie = filter_var($_POST["superficie"],FILTER_SANITIZE_NUMBER_FLOAT);
		} else {
		     $app->redirect($app->urlFor('notife', array(
	 		  	'msg' => 'Superficie non spécifié'
 		  	)));
		} 	


		// ------------ Type bien, type annonce, Quartier, Ville -----------------

		if(isset($_POST['id_typeB']) && filter_var($_POST['id_typeB'], FILTER_VALIDATE_INT)){
			$id_typeB = filter_var($_POST["id_typeB"],FILTER_SANITIZE_NUMBER_INT);
			$typBien = TypeBien::find($id_typeB);
			if( isset($typBien)){
				$id_typeB = $typBien->id;
			} else {
				  $app->redirect($app->urlFor('notife', array(
		 		  	'msg' => 'Type de bien éronné' 		  		
		 		  )));
			}
		} else {
		     $app->redirect($app->urlFor('notife', array(
	 		  	'msg' => 'Type de bien non définit'
 		  	)));
		} 	


		if(isset($_POST['id_typeA']) && filter_var($_POST['id_typeA'], FILTER_VALIDATE_INT)){
			$id_typeA = filter_var($_POST["id_typeA"],FILTER_SANITIZE_NUMBER_INT);
			$typAnn = TypeAnnonce::find($id_typeA);
			if( isset($typAnn)){
				$id_typeA = $typAnn->id;
			} else {
				  $app->redirect($app->urlFor('notife', array(
		 		  	'msg' => 'Type d\'annonce éronné'
 		  		)));
			}
		} else {
		     $app->redirect($app->urlFor('notife', array(
	 		  	'msg' => 'Type d\'annonce non définit'
 		  	)));
		} 


		if(isset($_POST['id_quart']) && filter_var($_POST['id_quart'], FILTER_VALIDATE_INT)){
			$id_quart = filter_var($_POST["id_quart"],FILTER_SANITIZE_NUMBER_INT);
			$quart = Quartier::find($id_quart);
			
			if( isset($quart)){
				$id_quart = $quart->id;
			} else {
				  $app->redirect($app->urlFor('notife', array(
		 		  	'msg' => 'Quartier éronné'
 		  		)));
			}
		} else {
		     $app->redirect($app->urlFor('notife', array(
	 		  	'msg' => 'Quartier non définit'
 		  	)));
		} 


		// ----------- Images -----------------

		$ret1 = false;
		$ret2 = false;
		$ret3 = false;

		$img1_url = ''; $img1_nom = '';
		$img2_url = ''; $img2_nom = '';
		$img3_url = ''; $img3_nom = '';
		$contentDir = "img/imgAnn/".date("Ym")."/";

		$img1_taille = 0; $img1_type = '';
		$img2_taille = 0; $img2_type = '';
		$img3_taille = 0; $img3_type = '';


		
		$ret1 = is_uploaded_file ($_FILES['fic1']['tmp_name']);
		$ret2 = is_uploaded_file ($_FILES['fic2']['tmp_name']);
		$ret3 = is_uploaded_file ($_FILES['fic3']['tmp_name']);

		$nb_img = 0 ;
		// Image 1
		if ( $ret1 ){

			// Le fichier a bien été reçu
			$img1_taille = $_FILES['fic1']['size'];
			if ( $_FILES['fic1']['error'] === 2){

		    	 $app->redirect($app->urlFor('notife', array(
		 		  	'msg' => 'Taille de l\'image 1 trop important'
 		  		)));
			}
			
			// Verification de l'extention
			$img1_type = $_FILES['fic1']['type'];
			if( !strstr($img1_type, 'jpg') && !strstr($img1_type, 'jpeg') && !strstr($img1_type, 'bmp') && !strstr($img1_type, 'gif') && !strstr($img1_type, 'png')){
				$app->redirect($app->urlFor('notife', array(
		 		  	'msg' => 'Le fichier 1 n\'est pas une image'
 		  		)));
			}

			$img1_nom = $_FILES['fic1']['name'];
			$tmp_file = $_FILES['fic1']['tmp_name'];
			if( !file_exists($contentDir) ){
				mkdir($contentDir, 0777,true);
			}
			
			// verification de l'upload
			if( !move_uploaded_file($tmp_file, $contentDir .$img1_nom ) ){
        		$app->redirect($app->urlFor('notife', array(
		 		  	'msg' => 'Impossible de copier le fichier dans notre base'
 		  		)));
    		}

			$img1_url = $contentDir . $img1_nom ;
			

			$nb_img++;
		} else {
		    $app->redirect($app->urlFor('notife', array(
	 		  	'msg' => 'Première image obligatoire non trouvé, veuillez re-télécharger l\'image'
 		  	)));
		}


		// Image 2 
		if ( $ret2 ){
			// Le fichier a bien été reçu
			$img2_taille = $_FILES['fic2']['size'];
			if ( $_FILES['fic2']['error'] === 2){

		    	 $app->redirect($app->urlFor('notife', array(
		 		  	'msg' => 'Taille de l\'image 2 trop important'
 		  		)));
			}
			
			// Verification de l'extention
			$img2_type = $_FILES['fic2']['type'];
			if( !strstr($img2_type, 'jpg') && !strstr($img2_type, 'jpeg') && !strstr($img2_type, 'bmp') && !strstr($img2_type, 'gif') && !strstr($img2_type, 'png')){
				$app->redirect($app->urlFor('notife', array(
		 		  	'msg' => 'Le fichier 2 n\'est pas une image'
 		  		)));
			}

			$img2_nom = $_FILES['fic2']['name'];
			$tmp2_file = $_FILES['fic2']['tmp_name'];
			if( !file_exists($contentDir) ){
				mkdir($contentDir, 0777,true);
			}
			
			// verification de l'upload
			if( !move_uploaded_file($tmp2_file, $contentDir .$img2_nom ) ){
        		$app->redirect($app->urlFor('notife', array(
		 		  	'msg' => 'Impossible de copier l\image dans notre base'
 		  		)));
    		}

			$img2_url = $contentDir . $img2_nom ;

			$nb_img++;
		} 

		// Image 3
		if ( $ret3 ){
			// Le fichier a bien été reçu
			$img3_taille = $_FILES['fic3']['size'];
			if ( $_FILES['fic3']['error'] === 2){

		    	 $app->redirect($app->urlFor('notife', array(
		 		  	'msg' => 'Taille de l\'image 3 trop important'
 		  		)));
			}
			
			// Verification de l'extention
			$img3_type = $_FILES['fic3']['type'];
			if( !strstr($img3_type, 'jpg') && !strstr($img3_type, 'jpeg') && !strstr($img3_type, 'bmp') && !strstr($img3_type, 'gif') && !strstr($img3_type, 'png')){
				$app->redirect($app->urlFor('notife', array(
		 		  	'msg' => 'Le fichier 3  n\'est pas une image'
 		  		)));
			}

			$img3_nom = $_FILES['fic3']['name'];
			$tmp3_file = $_FILES['fic3']['tmp_name'];
			if( !file_exists($contentDir) ){
				mkdir($contentDir, 0777,true);
			}
			
			// verification de l'upload
			if( !move_uploaded_file($tmp3_file, $contentDir .$img3_nom ) ){
        		$app->redirect($app->urlFor('notife', array(
		 		  	'msg' => 'Impossible de copier l\'image dans notre base'
 		  		)));
    		}

			$img3_url = $contentDir . $img3_nom ;

			$nb_img++;
		} 

		
		// ------------- Mot de passe --------------

		// Fonction d'optimisation de la génration du mot de passe
		if(!empty($_POST['mdpAnn'])){
			function getOptimalCost($timeTarget){ 
			    $cost = 9;
			    do {
			        $cost++;
			        $start = microtime(true);
			        password_hash("test", PASSWORD_BCRYPT, ["cost" => $cost]);
			        $end = microtime(true);
			    } while ( ($end - $start) < $timeTarget) ;
			    
			    return $cost;
			}
		
			$cost = getOptimalCost(0.3);
			$passwd = $_POST['mdpAnn'];
			$passwdhash = password_hash($passwd,PASSWORD_BCRYPT,['cost' => $cost]) ;

		} else {	
		     $app->redirect($app->urlFor('notife', array(
	 		  	'msg' => 'Mot de passe non spécifié'
 		  	)));
		} 
	}

	// ----------- Utilisateur ---------------
	// ------------ Nom / Pseudo --------------
	if(!empty($_POST['nomInter'])){
		$nomInter = filter_var($_POST['nomInter'], FILTER_SANITIZE_STRING);
	} else {
		 $app->redirect($app->urlFor('notife', array(
	 		  	'msg' => 'Nom/Pseudo non spécifié'
 		  	)));
	}

	// ----------------- Email -----------------
	if(!empty($_POST['emailInter']) && filter_var($_POST['emailInter'], FILTER_VALIDATE_EMAIL)){
		$emailInter = filter_var($_POST['emailInter'], FILTER_SANITIZE_EMAIL);
	} else {
		 $app->redirect($app->urlFor('notife', array(
	 		  	'msg' =>  'Email non spécifié' 		  	
	 		  )));
	}

	if(!empty($_POST['telInter'])){
		$telInter = filter_var($_POST['telInter'], FILTER_SANITIZE_NUMBER_INT);
	} else {
		 $app->redirect($app->urlFor('notife', array(
	 		  	'msg' => 'Téléphone non spécifié'
 		  	)));
	}

   
   // Interation avec la base de donnée

	$internaute = new Internaute();
	$internaute->emailInter = $emailInter;
	$internaute->telInter = $telInter;
	$internaute->nomInter = $nomInter;
	$internaute->save();

	$annonce = new Annonce();

	$annonce->titre = $titre;
	$annonce->contenu = $contenu;
	$annonce->prix = $prix;
	$annonce->loyer = $loyer;
	$annonce->nbPiece = $nbPieces;
	$annonce->superficie = $superficie;
	$annonce->mdpAnn = $passwdhash;

	$annonce->inter_id  =  $internaute->id;
	$annonce->typeAnn_id = $id_typeA;
	$annonce->typeBien_id = $id_typeB;
	$annonce->quartier_id = $id_quart;
	$annonce->save();
	

	for($i=1;$i<=$nb_img;$i++){
		${'image'.$i} = new Image();
		${'image'.$i}->imageChemin = ${'img'.$i.'_url'};
		${'image'.$i}->imageName = ${'img'.$i.'_nom'};
		${'image'.$i}->imageType = ${'img'.$i.'_type'};
		${'image'.$i}->annonce_id = $annonce->id;
		${'image'.$i}->save();
	}


	$app->render('notification.html', array(
		'msg' => 'Votre annnonce a été déposé avec succès',
		'coulAlert' => 'alertSuccess',
		'titreAlert' => 'Succès'
	));
});




// Les pages de notifications

$app->get('/danger:msg', function ($msg ='') use ($app) {

	$app->render('notification.html', array(
		'msg' => $msg,
		'coulAlert' => 'alertDanger',
		'titreAlert' => 'Attention !'
	));
})->name('notifd');

$app->get('/success:msg', function ($msg ='', $coulAlert='',$titreAlert='') use ($app) {

	$app->render('notification.html', array(
		'msg' => $msg,
		'coulAlert' =>'alertSuccess',
		'titreAlert' => 'Succès'
	));
})->name('notifs');


$app->get('/info:msg', function ($msg ='', $coulAlert='',$titreAlert='') use ($app) {

	$app->render('notification.html', array(
		'msg' => $msg,
		'coulAlert' => 'alertInfo',
		'titreAlert' =>'Information'
	));
})->name('notifi');

$app->get('/erreur:msg', function ($msg ='',$titreAlert='') use ($app) {
	// var_dump($coulAlert);
	$app->render('notification.html', array(
		'msg' => $msg,
		'coulAlert' =>'alertAdvert',
		'titreAlert' =>'Erreur'
	));
})->name('notife');


?>

