-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost:3306
-- Généré le :  Mar 20 Octobre 2015 à 11:35
-- Version du serveur :  5.5.42
-- Version de PHP :  5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `magicimmo`
--
-- --------------------------------------------------------
CREATE DATABASE IF NOT EXISTS `magicimmo` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `magicimmo`;



--
-- Structure de la table `typeBien`
--

DROP TABLE IF EXISTS `image`;
CREATE TABLE IF NOT EXISTS `image` (
  `id` int(11) NOT NULL,
  `imageChemin` varchar(250) NOT NULL,
  `imageName` varchar(250) NOT NULL,
  `imageType` varchar(100) NOT NULL,
  `annonce_id` int(11) NOT NULL,

  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=7;


--
-- Structure de la table `Annonce`
--

DROP TABLE IF EXISTS `annonce`;
CREATE TABLE IF NOT EXISTS `annonce` (
  `id` int(11) NOT NULL,
  `titre` varchar(200) NOT NULL,
  `contenu` text NOT NULL, 
  `prix` double(10,2) NOT NULL,
  `loyer` double(10,2) NOT NULL,
  `nbPiece` int(3) NOT NULL,
  `superficie` double(10,3) NOT NULL,
  `mdpAnn` varchar(50) NOT NULL,

  `inter_id` int(11) NOT NULL,
  `typeAnn_id` int(11) NOT NULL,
  `typeBien_id` int(11) NOT NULL,
  `quartier_id` int(11) NOT NULL,

  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=0 ;

--
-- Structure de la table `internaute`
--

DROP TABLE IF EXISTS `internaute`;
CREATE TABLE IF NOT EXISTS `internaute` (
  `id` int(11) NOT NULL ,
  `emailInter` varchar(80) NOT NULL,
  `nomInter` varchar(80) NOT NULL,
  `telInter` varchar(80) NOT NULL,

  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=0;


--
-- Structure de la table `typeAnnonce`
--

DROP TABLE IF EXISTS `typeAnnonce`;
CREATE TABLE IF NOT EXISTS `typeAnnonce` (
  `id` int(11) NOT NULL,
  `nomTypeAnn` varchar(128) NOT NULL,

  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3;


--
-- Contenu de la table `typeAnnonce`
--

INSERT INTO `typeAnnonce` (`id`, `nomTypeAnn`, `updated_at`, `created_at`) VALUES
(1, 'Location', '2015-10-20 09:00:56', '2015-10-18 21:58:59'),
(2, 'Vente', '2015-10-20 09:01:01', '2015-10-18 22:12:28');



--
-- Structure de la table `typeBien`
--

DROP TABLE IF EXISTS `typeBien`;
CREATE TABLE IF NOT EXISTS `typeBien` (
  `id` int(11) NOT NULL,
  `nomTypeBien` varchar(128) NOT NULL,

  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3;

--
-- Contenu de la table `typeBien`
--

INSERT INTO `typeBien` (`id`, `nomTypeBien`, `updated_at`, `created_at`) VALUES
(1, 'Maison', '2015-10-20 09:00:56', '2015-10-18 21:58:59'),
(2, 'Appartement', '2015-10-20 09:01:01', '2015-10-18 22:12:28'),
(3, 'Local', '2015-10-20 09:01:01', '2015-10-18 22:12:28'),
(4, 'Terrain', '2015-10-20 09:01:01', '2015-10-18 22:12:28'),
(5, 'Parking', '2015-10-20 09:01:01', '2015-10-18 22:12:28'),
(6, 'Autres', '2015-10-20 09:01:01', '2015-10-18 22:12:28');




--
-- Structure de la table `quartier`
--

DROP TABLE IF EXISTS `quartier`;
CREATE TABLE IF NOT EXISTS `quartier` (
  `id` int(11) NOT NULL ,
  `nomQuartier` varchar(128) NOT NULL,
  `ville_id` int(11) NOT NULL,

  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=7;

--
-- Contenu de la table `quartier`
--

INSERT INTO `quartier` (`id`, `nomQuartier`,`ville_id` ,`updated_at`, `created_at`) VALUES
(1, 'Médreville', 1,'2015-10-20 09:00:56', '2015-10-18 21:58:59'),
(2, 'Saint-Nicolas',1 ,'2015-10-20 09:01:01', '2015-10-18 22:12:28'),
(3, 'Centre-Ville', 1,'2015-10-20 09:01:01', '2015-10-18 22:12:28'),
(4, 'Les provinces',2 ,'2015-10-20 09:01:01', '2015-10-18 22:12:28'),
(5, 'Vélodrome',3,'2015-10-20 09:01:01', '2015-10-18 22:12:28'),
(6, 'Boudonville',2,'2015-10-20 09:01:01', '2015-10-18 22:12:28');


--
-- Structure de la table `ville`
--

DROP TABLE IF EXISTS `ville`;
CREATE TABLE IF NOT EXISTS `ville` (
  `id` int(11) NOT NULL ,
  `nomVille` varchar(128) NOT NULL,

  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6;

--
-- Contenu de la table `ville`
--

INSERT INTO `ville` (`id`, `nomVille`, `updated_at`, `created_at`) VALUES
(1, 'Nancy', '2015-10-20 09:00:56', '2015-10-18 21:58:59'),
(2, 'Laxou', '2015-10-20 09:01:01', '2015-10-18 22:12:28'),
(3, 'Vandoeuvre', '2015-10-20 09:01:01', '2015-10-18 22:12:28'),
(4, 'Saint-Max', '2015-10-20 09:01:01', '2015-10-18 22:12:28'),
(5, 'Maxéville', '2015-10-20 09:01:01', '2015-10-18 22:12:28');




--
-- Primary & foreign key
--
ALTER TABLE `internaute`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `annonce`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `typeBien`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `typeAnnonce`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `ville`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `quartier`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

  -- AUTO INCREMENT 

ALTER TABLE `internaute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=0;

ALTER TABLE `annonce`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=0;

ALTER TABLE `typeBien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;

ALTER TABLE `typeAnnonce`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;

ALTER TABLE `ville`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;

ALTER TABLE `quartier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;

ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=0;



-- Foreign key 

ALTER TABLE `annonce`
  ADD CONSTRAINT `fk_ann_quart` FOREIGN KEY (`quartier_id`) REFERENCES quartier(`id`),
  ADD CONSTRAINT `fk_ann_inter` FOREIGN KEY (`inter_id`) REFERENCES internaute(`id`),
  ADD CONSTRAINT `fk_ann_tyBi` FOREIGN KEY (`typeBien_id`) REFERENCES typeBien(`id`),
  ADD CONSTRAINT `fk_ann_typeAnn` FOREIGN KEY (`typeAnn_id`) REFERENCES typeAnnonce(`id`);

ALTER TABLE `quartier`
     ADD CONSTRAINT `fk_quar_ville`  FOREIGN KEY (`ville_id`) REFERENCES ville(`id`);

ALTER TABLE `image`
    ADD CONSTRAINT `fk_img_ann` FOREIGN KEY (`annonce_id`) REFERENCES annonce(`id`);